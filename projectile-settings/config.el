;; some projectile settings taken from Joe
;; from https://joshwolfe.ca/post/emacs-for-csharp/
(setq projectile-enable-caching t)
(setq projectile-globally-ignored-file-suffixes
      '("#" "~" ".swp" ".o" ".so" ".exe" ".dll" ".elc" ".pyc" ".jar"))
(setq projectile-globally-ignored-directories
      '(".git" "node_modules" "__pycache__" ".vs"))
(setq projectile-globally-ignored-files '("TAGS" "tags" ".DS_Store"))
