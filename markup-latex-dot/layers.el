(configuration-layer/declare-layer 'asciidoc)
(configuration-layer/declare-layer 'bibtex)
(configuration-layer/declare-layer 'csv)
(configuration-layer/declare-layer 'graphviz)
(configuration-layer/declare-layer 'html)
(configuration-layer/declare-layer 'latex)
(configuration-layer/declare-layer 'markdown)
(configuration-layer/declare-layer 'pandoc)
(configuration-layer/declare-layer 'plantuml)
(configuration-layer/declare-layer 'yaml)


(setq org-plantuml-jar-path
      (expand-file-name "/usr/share/plantuml/plantuml.jar"))
