;; disable watching files
(setq lsp-enable-file-watchers nil)
(setq projectile-git-submodule-command nil)
