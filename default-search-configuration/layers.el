(defun lu-helm-search-find-with-fzf-and-fd ()
  (interactive)
  (helm :sources (
                   helm-build-async-source "fzf-find-files"
                    :candidates-process
                    (lambda ()
                      start-process "echo" "buffer-echo" "echo" "a\nb\nc\n"
                      )
                   )
        :buffer "*helm lu find files with fzf*"))

(defun insert-puppies-output ()
  "Insert my command output into the buffer."
  (interactive)
  (sert-shell "echo 'puppies'"))
