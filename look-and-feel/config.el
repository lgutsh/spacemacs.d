;; always show line numbers
(setq dotspacemacs-line-numbers t)

;; Do not use q for macros
;; I tend to press it unintentionaly instead of j
;; I maybe should reconsider remapping of sequence of these four symbols to something different: 'qjk 
;; (unbind-key (kbd "q") evil-normal-state-map)
